#! /usr/bin/env python

import sys
import os
import subprocess
import time
import re
import json
import datetime

SSH_KEY='/.ssh/id_rsa'

def filter_non_printable(str):
  return ''.join([c for c in str if ord(c) > 31 or ord(c) == 9])

class pool:
    def list(self, hostname):
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, 'nstpool list']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        list = []
        for line in p.stdout:
            line = filter_non_printable(line)
            if line.find('NAME') and line.find('nstbootpool') and line.find('[?1000l'):
                if line.split()[0]:
                    list.append(line.split()[0])
            p.wait()
        return list
    def iops(self, hostname, pool):
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, 'nstpool iostat', pool, '1 4']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        for line in p.stdout:
            if pool in line:
                read = line.split()[3]
                if 'K' in read:
                    read = 1024 * float(read.replace('K', ''))
                read = int(read)
                write = line.split()[4]
                if 'K' in write:
                    write = 1024 * float(write.replace('K', ''))
                write = int(write)
                total = read + write
            p.wait()
        return (read, write, total)
"""
    def bandwidth(self):
    def average_io_size(self):
    def latency(self):
    def all(self):
"""

class network_port:
    def list(self, hostname):
        return [ 'nx0' ]
    def iops(self, hostname, nic):
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, 'nic show-link -s', nic]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        for line in p.stdout:
            if nic in line:
                read1 = int(line.split()[4])
                write1 = int(line.split()[1])
                total1 = read1 + write1
            p.wait()
        sleep_range = 5
        time.sleep(sleep_range)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        for line in p.stdout:
            if nic in line:
                read2 = int(line.split()[4])
                write2 = int(line.split()[1])
                total2 = read2 + write2
            p.wait()
        read = (read2 - read1) / sleep_range
        write = (write2 - write1) / sleep_range
        total = read + write
        return (read, write, total)
"""
    def bandwidth(self):
    def average_io_size(self):
    def latency(self):
    def all(self):
"""
class cifs_share:
    def list(self, hostname):
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, 'shares'] 
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        list = []
        for line in p.stdout:
            line = filter_non_printable(line)
            if line.find('[?1000l') and not line.find('Share  :'):
                if line.split()[1]:
                    list.append(line.split()[2])
            p.wait()
        return sorted(set(list))
    def iops(self, hostname, shares):
        #trace -c run -t cifssvrtop -a -j 1 1
        #{ "collector": "cifssvrtop", "time": "2013 Apr 25 03:46:35", "timestamp": 1366886795479024451, "interval": 10, "load": 1.13, "read_KB_int": 0, "write_KB_int": 1089456, "clientdata": [{"share": "cifs_test", "client": "10.60.61.31", "CIFSOPS": 843, "reads": 0, "writes": 843, "read_bw": 0, "write_bw": 53996, "avg_read_t": 0, "avg_write_t": 79, "aligned_pct": 100 },{"share": "test", "client": "10.60.61.31", "CIFSOPS": 857, "reads": 0, "writes": 857, "read_bw": 0, "write_bw": 54846, "avg_read_t": 0, "avg_write_t": 80, "aligned_pct": 100 },{}]}
        cifssvrtop_cmd = 'trace -c run -t cifssvrtop -a -j 1 3'
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, cifssvrtop_cmd] 
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        for line in p.stdout:
            if '"collector": "cifssvrtop"' in line:
                try:
                    data = json.loads(line)
                except:
                    print "Failed to decode json - bad input"
                    pass
            p.wait()
        shares_list = {}
        for share_name in shares:
            shares_list[share_name] = []
        clients_list = []
        cifs_io = 0
        for share in enumerate(data['clientdata']):
            if share[1]:
                #(1, {u'avg_read_t': 0, u'CIFSOPS': 930, u'read_bw': 0, u'writes': 930, u'share': u'cifs_test', u'aligned_pct': 100, u'reads': 0, u'client': u'10.60.61.31', u'avg_write_t': 84, u'write_bw': 59520})		        
                cifs_io = 1
                share_stats = share[1]
                share_name = share_stats['share']
                shares_list[share_name].append({
                                                        'client': share_stats['client'], 
                                                        'reads': share_stats['reads'], 
                                                        'writes': share_stats['writes'], 
                                                        'total': share_stats['CIFSOPS']})
                """
                clients_list{share_stats['client']}.append({
                                                        'share': share_stats['share'], 
                                                        'reads': share_stats['reads'], 
                                                        'writes': share_stats['writes'], 
                                                        'total': share_stats['CIFSOPS']})
                """
                print '%s - iops share_stats: %s, client: %s, read: %s, write: %s, total: %s' % (datetime.datetime.now().isoformat(), share_stats['share'], share_stats['client'], share_stats['reads'], share_stats['writes'], share_stats['CIFSOPS'])
        if cifs_io == 0:
            print '%s - iops share_stats: no_cifs_io, client: no_cifs_io, read: 0, write: 0, total: 0' % (datetime.datetime.now().isoformat())

        #print "==> shares_list: "
        #print shares_list
        #print "================="
        for share in shares_list:
            #print "==> share: "
            #print "%s : %s" % (share, shares_list[share])
            #print "================="
            share_stats = share
            """
                reads = reads + client['reads']
                writes = writes + client['writes']
                CIFSOPS = CIFSOPS + client['CIFSOPS']
            """
            #print '%s - iops share_stats: %s, client: %s, read: %s, write: %s, total: %s' % (datetime.datetime.now().isoformat(), share_stats['share'], 'all', share_stats['reads'], share_stats['writes'], share_stats['CIFSOPS'])
        return shares_list, clients_list
"""
    def bandwidth(self):
    def average_io_size(self):
    def latency(self):
    def all(self):
"""
class lun: 
    def list(self, hostname):
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, 'iostat -xn']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        list = []
        for line in p.stdout:
            line = line.replace('\r\n', '\n')
            if re.search(' c.t.{5,}d.$', line):
                list.append(line.split()[10])
            p.wait()
        return list
    def iops(self, hostname, lun):
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, 'iostat -xn', lun, '1 3']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        for line in p.stdout:
            line = line.replace('\r\n', '\n')
            if re.search(' c.t.{5,}d.$', line):
                read = line.split()[0]
                read = int(float(read))
                write = line.split()[1]
                write = int(float(write))
                total = read + write
            p.wait()
        return (read, write, total)

class FASTier_read_stat:
    def all(self, hostname):
        cmd_arcstat = "trace -c run -t arcstat -a -f 'read,hits,miss,hit%,l2read,l2hits,l2miss,l2hit%,arcsz,l2size'"
        cmd = ['ssh', '-i', SSH_KEY, '-l', 'nxadmin', hostname, cmd_arcstat]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        i = 0
        for line in p.stdout:
            if i == 2:
                read = line.split()[0]
                hits = line.split()[1]
                miss = line.split()[2]
                hit_rate = line.split()[3]
                l2read = line.split()[4]
                l2hits = line.split()[5]
                l2miss = line.split()[6]
                l2hit_rate = line.split()[7]
                arcsz = line.split()[8]
                l2size = line.split()[9]
            p.wait()
            i = i + 1
        print "%s - FASTier Read Stats: read: %s, hits: %s, miss: %s, hit%%: %s, l2read: %s, l2hits: %s, l2miss: %s, l2hit%%: %s, arcsz: %s, l2size: %s" % (datetime.datetime.now().isoformat(), read, hits, miss, hit_rate, l2read, l2hits, l2miss, l2hit_rate, arcsz, l2size)
        return 0
"""
    def bandwidth(self):
    def average_io_size(self):
        (kr/s)/(r/s) = x kB reads
        (kw/s)/(r/s) = x kB writes
    def latency(self):
    def all(self):
"""
"""
class cluster: (one or n controllers)
    def list(self, hostname):
    def iops(self, hostname, pool):
    def bandwidth(self):
    def average_io_size(self):
    def latency(self):
    def all(self):
class lun: have to specify a pool
    def list(self, hostname):
    def iops(self, hostname, pool):
    def bandwidth(self):
    def average_io_size(self):
    def latency(self):
    def all(self):
class disk:
    def list(self, hostname):
    def iops(self, hostname, pool):
    def bandwidth(self):
    def average_io_size(self):
    def latency(self):
    def all(self):
"""

if __name__ == '__main__':
    import optparse
    parser = optparse.OptionParser()
    parser.add_option('-s', '--hostname',
                      dest='ip',
                      help='storage controller hostname or ip address')
    parser.add_option('-o', '--output',
                      dest='output',
                      default='default',
                      help='default|munin|json')
    parser.add_option('-a', '--all',
                      dest='all_stats',
                      action='store_true',
                      default=False,
                      help='pull all layer stats -- protocol access, nic, cluster, nstpool, LUN, disk  (default false)')
    parser.add_option('-n', '--nic',
                      dest='nic_stats',
                      action='store_true',
                      default=False,
                      help='pull the nic stats (default false)')
    parser.add_option('-c', '--share',
                      dest='share_stats',
                      action='store_true',
                      default=False,
                      help='pull the cifs stats (default false)')
    parser.add_option('-p', '--pool',
                      dest='pool_stats',
                      action='store_true',
                      default=False,
                      help='pull the pool stats (default false)')
    parser.add_option('-l', '--lun',
                      dest='lun_stats',
                      action='store_true',
                      default=False,
                      help='pull the lun stats (default false)')
    parser.add_option('-z', '--cache',
                      dest='cache_stats',
                      action='store_true',
                      default=False,
                      help='pull the cache stats -- FASTier write and read (default false)')
    parser.add_option('-t', '--stats_type',
                      dest='stats_type',
                      default='iops',
                      help='all|list|iops')
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    options, args = parser.parse_args(sys.argv[1:])
    if not options.ip:
        parser.print_help()
        parser.error('You have to specify a NST hostname or IP address using the -s parameter')
    if options.nic_stats or options.all_stats:
        nx_port = network_port()
        for nic in nx_port.list(options.ip):
            (read, write, total) = nx_port.iops(options.ip, nic)
            print "%s - iops nic: %s, read:%s, write:%s, total:%s" % (datetime.datetime.now().isoformat(), nic, read, write, total)
    if options.share_stats or options.all_stats:
	if options.stats_type == 'iops' or options.stats_type == 'all':
        	share = cifs_share()
        	share.iops(options.ip, share.list(options.ip))
	if options.stats_type == 'list' or options.stats_type == 'all':
        	share = cifs_share()
        	for i in share.list(options.ip):
			print i
    if options.pool_stats or options.all_stats:
        subsystem = pool()
        for pool in subsystem.list(options.ip):
            (read, write, total) = subsystem.iops(options.ip, pool)
            print "%s - iops pool: %s, read:%s, write:%s, total:%s" % (datetime.datetime.now().isoformat(), pool, read, write, total)
    if options.lun_stats or options.all_stats:
        subsystem = lun()
        for lun in subsystem.list(options.ip):
            (read, write, total) = subsystem.iops(options.ip, lun)
            print "%s - iops lun: %s, read:%s, write:%s, total:%s" % (datetime.datetime.now().isoformat(), lun, read, write, total)
    if options.cache_stats or options.all_stats:
        cache_read_stat = FASTier_read_stat()
        cache_read_stat.all(options.ip)
    sys.exit(0)
